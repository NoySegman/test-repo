from fastapi import FastAPI
import uvicorn


class HTTPServer:
    def __init__(self, ip, port):
        self.app = FastAPI()
        self.host = ip
        self.port = port
        self.app.add_api_route(path='/ping', methods=["GET"], endpoint=self.ping)

    def ping(self):
        return "pong"

    def start(self):
        uvicorn.run(self.app, host=self.host, port=self.port)

    def stop(self):
        pass


if __name__ == '__main__':
    server = HTTPServer(ip="127.0.0.1", port=7085)
    server.start()
